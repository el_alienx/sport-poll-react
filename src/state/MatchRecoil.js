// React core
import { atom, selector } from 'recoil';

// Logic
import MatchHandler from "utils/MatchHandler"

// State
export const indexState = atom(
  {
    key: 'index',
    default: 0
  }
)

export const matchesState = atom({
  key: 'matches',
  default: [MatchHandler.filterMatches(require("assets/test-assignment.json"))] // must explicitively cast as an array otherwise will generate a un explicit error
})