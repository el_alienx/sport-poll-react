import IMatch from "interfaces/IMatch"
import eSports from "interfaces/eSports"
import Utils from "utils/Utils"

export default abstract class MatchHandler {
  // Pure
  public static filterMatches(matches: Array<IMatch>, optionalCategory?: eSports): IMatch[] {
    const randomCategory = this._randomizeCategory();
    const category = optionalCategory ? optionalCategory : randomCategory

    return matches.filter(match => match.sport === category);
  }

  // Pure
  private static _randomizeCategory(): eSports {
    const categories: eSports[] = Object.values(eSports);
    const index: number = Utils.randomizer(categories.length); // move randomize to this store

    return categories[index];
  }
}