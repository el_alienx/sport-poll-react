import IUserVote from "interfaces/IUserVote"

export default abstract class MatchState {
  private static _sessionName = "userVotes"

  private static readonly _data = {
    userVotes: Array<IUserVote>()
  }

  // Impure (looks like a pure getter, but this allows to set the value)
  public static data() {
    return this._data
  }

  // Impure
  public static loadSession() {
    const files = sessionStorage.getItem(this._sessionName)
    const newVotes: IUserVote[] = []

    this._data.userVotes = (files !== null) ? JSON.parse(files) : newVotes
  }

  //  Impure (depend on userVotes)
  public static isExistingVote(vote: IUserVote): Boolean {
    const userVotes = this._data.userVotes
    const queryVote = userVotes.find(({ objectId }) => objectId === vote.objectId)

    return queryVote !== undefined ? true : false
  }

  //  Impure (depend on userVotes)
  public static storeVote(newVote: IUserVote) {
    const userVotes = this._data.userVotes
    const currentVote = userVotes.find(({ objectId }) => objectId === newVote.objectId)

    if (currentVote === undefined) {
      userVotes.push(newVote)
    }
    else {
      currentVote.voteType = newVote.voteType
    }

    this._saveSession()
  }

  // Impure (but is ok, we need to mutate the session state)
  private static _saveSession() {
    const jsonString = JSON.stringify(this._data.userVotes)

    sessionStorage.setItem(this._sessionName, jsonString)
  }
}