export default class Utils {

  // API
  // Pure
  public static randomizer(limit: number, previousNumber?: number) {
    let newNumber = Math.floor(Math.random() * limit);

    if (newNumber === previousNumber) {
      if (newNumber === 0) newNumber = limit - 1;
      else newNumber = newNumber - 1;
    }

    return newNumber
  }
}