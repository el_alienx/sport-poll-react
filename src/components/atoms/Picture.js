// React core
import React from 'react';

export default function Picture({ prop }) {
  return (
    <img className="picture" src={_requestImage(prop)} alt={prop} />
  )
}

function _requestImage(fileName) {
  const parsedName = fileName.replace("/", " and ");
  let result = "";

  try {
    result = require(`assets/images/${parsedName}.png`);
  } catch (error) {
    result = require(`assets/images/placeholder.png`);
  }

  return result;
}