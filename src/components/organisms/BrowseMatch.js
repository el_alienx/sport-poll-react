// React core
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';

// Components
import Card from "components/molecules/Card"

// State
import { matchesState, indexState } from "state/MatchRecoil"

export default function BrowseMatch() {
  // State
  const [matches] = useRecoilValue(matchesState)
  const [index, setIndex] = useRecoilState(indexState);


  // Methods
  function chooseMatch(newIndex) {
    setIndex(newIndex)
  }

  // Pre-rendered compoennt
  const cards = matches.map((match, index) => {
    return <Card key={match.id} onClick={() => chooseMatch(index)} match={match} />
  })

  return (
    <section className="section browse-match">
      <h2 className="padding-sides" key={index}>Vote on similar games</h2>

      <div className="scroller">
        {cards}
      </div>
    </section>
  )
}