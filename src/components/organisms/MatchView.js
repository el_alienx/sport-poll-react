// React core
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';

// Logic
import Utils from 'utils/Utils';
import UserState from 'utils/UserState'

// Components
import MatchDetail from "components/molecules/MatchDetail"
import VoteControl from 'components/molecules/VoteControl';

// State
import { indexState, matchesState, currentMatchState } from "state/MatchRecoil"

export default function MatchView() {
  // State
  const [matches] = useRecoilValue(matchesState)
  const [index, setIndex] = useRecoilState(indexState)

  // Old State
  const hasVoted = alreadyVoted()

  // Computed propertues
  function alreadyVoted() {
    const checkVote = { objectId: matches[index].objectId, voteType: 0 };
    return UserState.isExistingVote(checkVote)
  }

  // Methods
  function onVote(voteType) {
    const newVote = { objectId: matches[index].objectId, voteType: voteType };
    UserState.storeVote(newVote);
    setIndex(Utils.randomizer(matches.length, index));
    alert("Thank you for voting, good luck!");
  }

  return (
    < section className="section match-view" >
      <MatchDetail prop={matches[index]} />
      <hr />
      <VoteControl match={matches[index]} hasVoted={hasVoted} onVote={onVote} />
    </section >
  )
}