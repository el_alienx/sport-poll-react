// React core
import React from 'react';

// Components
import Picture from "components/atoms/Picture"

export default function Card({ onClick, match }) {
  return (
    <article className="card" onClick={onClick}>
      <Picture prop={match.homeName} />
      <Picture prop={match.awayName} />
      <br />
      {match.name}
    </article>
  )
}