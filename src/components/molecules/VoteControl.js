// React core
import React from 'react';

export default function VoteControl({ match, hasVoted, onVote }) {
  // Data
  const labels = [match.homeName, "Draw", match.awayName];

  // Pre-rendered component
  const voteOptions = labels.map((label, index) => (
    <div className="column" key={index}>
      <button onClick={() => onVote(index)}>{label}</button>
    </div>
  ))

  return (
    <section>
      <div className={`vote-control ${!hasVoted ? 'show' : 'hide'}`}>
        <h2>Who wins?</h2>
        <div className="columns">
          {voteOptions}
        </div >
      </div>

      <p className={hasVoted ? 'show' : 'hide'}>
        You already voted, crossing fingers
        <span role="img" aria-label="fingers crossed emoji">🤞</span>
        so you can win
      </p>
    </section>
  )
}