// React core
import React from 'react';

// Components
import Picture from "../atoms/Picture"

export default function MatchView({prop}) {
  return (
    <section className="match-detail">
      <span className="category">{prop.sport}</span>
      <h1>{prop.group} - {prop.country}</h1>

      <div className="columns matain-columns">
        <div className="column">
          <Picture prop={prop.homeName} />
          <p className="name">{prop.homeName}</p>
        </div>

        <div className="column vertical-align">
          <span className="vs">VS.</span>
        </div>

        <div className="column">
          <Picture prop={prop.awayName} />
          <p className="name">{prop.awayName}</p>
        </div>
      </div>
    </section>
  )
}
