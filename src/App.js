// React core
import React from 'react';
import { RecoilRoot } from 'recoil';

// Assets and style
import logo from 'assets/icons/logo.svg'
import "styles/style.sass"

// Components
import BrowseMatch from "components/organisms/BrowseMatch"
import MatchView from "components/organisms/MatchView"

export default function App() {
  return (
    <RecoilRoot>
      <div className="App">
        <header className="header padding-sides">
          <img className="icon" src={logo} alt="Sport Poll logo" />Sport Poll
      </header>

        <div className="container vertical-align">
          <MatchView />
          <BrowseMatch />
        </div>
      </div>
    </RecoilRoot>
  );
}
