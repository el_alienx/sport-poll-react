export default interface IUserVote {
  objectId: String,
  voteType: Number
}
